#!/usr/bin/env python3
import requests
import json
import urllib
from datetime import datetime
# from datetime import date
# from datetime import time
from requests.auth import HTTPBasicAuth

def get_observer_location():
    URL = "http://ip-api.com/json/"
    r = requests.get(url = URL) 
    data = r.json()
    print(json.dumps(data, indent=3))

    latitude = data['lat']
    longitude = data['lon']
    city = data['city']
    regionName = data['regionName']
    return latitude, longitude

#print(get_observer_location())

print('#'*49)

def get_date():

    current_date = datetime.now().date()
    return str(current_date)

def get_time():
    return datetime.now().strftime("%H:%M:%S")

def get_sun_location():
    ASTRONOMYAPI_ID = "79d6a51d-241f-4ce6-b887-302d9d8ed792"
    ASTRONOMYAPI_SECRET = "3c11b028cb19d16c7453a070f7a236ad8837fdf37b38342f23ff7b7c79fcb40cdceca40153db5ec09171aa1e5acc47244e7dddeed2a54cd07e0231794ca2e8967d7e3bd4aeec23101cd4358c31983e68a647b1e4ce1485f33299cc3c170168b94bf889df985d79eb79140d24473c4764"
    url = "https://api.astronomyapi.com/api/v2/bodies/positions"
    auth = ('ASTRONOMYAPI_ID', 'ASTRONOMYAPI_SECRET')
    latitude, longitude = get_observer_location()
    from_date = to_date = get_date()
    time = get_time()
    params = {"latitude" :latitude,"longitude": longitude, "elevation":"0", "from_date": from_date,"to_date": to_date,"time": time }
    r = requests.get(url = url,params=params, auth = HTTPBasicAuth(ASTRONOMYAPI_ID, ASTRONOMYAPI_SECRET, ))
    data = r.json()
    #print(json.dumps(data, indent=3))
    for raw in data['data']['table']['rows']:
        for cell in raw['cells']:
            if cell['name'] == "Sun":
                sun_horizontal = cell['position']['horizonal']
                return sun_horizontal['azimuth']['degrees'], sun_horizontal['altitude']['degrees']
#print(get_sun_location( ))


def print_position(azimuth, altitude):
    azimuth, altitude = get_sun_location()
    print('The sun is currently at',azimuth,'degrees azimuth', altitude,'degrees altitude')



if __name__ =="__main__":
    latitude, longitude = get_observer_location()
    azimuth, altitude = get_sun_location()
    print_position(azimuth, altitude)
